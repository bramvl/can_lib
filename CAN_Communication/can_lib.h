/*
 * can_lib.h
 *
 *  Created on: Nov 15, 2013
 *      Author: BramVL
 *     Version: 1.1
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>


#ifndef CAN_LIB_H_
#define CAN_LIB_H_

#define DEBUG 1 //if 1 then debug output will be enabled
//#define OUTPUT 0

//******************* ID's FOR CAN **********************//

#define ID_REMMEN 			1
#define ID_GAS				2
#define ID_VERSNELLING		3
#define ID_LICHTEN			4
#define ID_RUITENWISSERS 	5
#define ID_KLAXON			6
#define ID_CV				7

//*******************************************************//
#ifdef OUTPUT
FILE *logfp;
#endif

//************************************ FUNCTIONS ************************************//

int load_can_modules(int); //returns 0 if correct. -1 if an error occurs
int init_can_socket(struct sockaddr_can*,int*, struct ifreq *ifr,int loopbackOn); //returns 0 if correct. -1 if an error occurs
int bind_can_socket(int*, struct sockaddr_can); //returns 0 if correct. -1 if an error occurs
int write_can_frame_ascii(int*, struct sockaddr_can*,int,unsigned char*); //returns length of the data, -1 if an error occurs

int write_can_frame(int *,struct sockaddr_can*,int,__u8[8]);

int read_can_frame(int*, struct sockaddr_can*,__u8*,int); //return 1 if the received packet is for the listening ID 0 if it is not and -1 if an error occurs
void error_report(int); //exits the program if an error occurs

//***********************************************************************************//
#endif /* CAN_LIB_H_ */
