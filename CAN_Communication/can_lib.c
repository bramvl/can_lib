/*
 * can_lib.c
 *
 *  Created on: Nov 15, 2013
 *      Author: BramVL
 *     Version: 1.1
 */

#include "can_lib.h"

//TODO: Log file;

// Error function
void error_report(int status){
	if(status < 0){
		perror("#FAIL:");
		exit(-1);
	}
	return;
}


//***************************************//
// loopback	= enables the loopback
//		  interface
//**************************************//
int load_can_modules(int loopback){

	int status;

	status = system("sudo rmmod mcp251x"); //Removing this module allows resetting the can network

	status = system("sudo modprobe spi-bcm2708");
	status = system("sudo modprobe can");
	status = system("sudo modprobe can-dev");
	status = system("sudo modprobe can-raw");
	status = system("sudo modprobe can-bcm");
	status = system("sudo modprobe mcp251x");
	error_report(status);
	printf("#MODULES LOADED SUCCESFULL\n");

	if(loopback)
	{
		status = system("sudo ip link set can0 type can bitrate 125000 loopback on");
	}
	else
		status = system("sudo ip link set can0 type can bitrate 125000");
	status = system("sudo ifconfig can0 up");
	error_report(status);
	printf("#PUTTING UP INTERFACE SUCCESFULL\n");

	return 0;
}

//*************************************************//
// sockaddr_can = struct used to store can server
//                properties
// int *s       = pointer to the socket
// struct ifreq = struct used to store interface
//                name
//*************************************************//
int init_can_socket(struct sockaddr_can* addr,int *s,struct ifreq *ifr,int loopbackOn){
	*s = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if(*s < 0) return -1;
	addr->can_family = AF_CAN;
	strcpy(ifr->ifr_name,"can0");
	if (ioctl(*s, SIOCGIFINDEX, ifr) < 0) {
		return -1;
	}

#ifdef OUTPUT
	logfp = fopen("can_log.txt","rw");
	fprintf(logfp,"logfile created\n");
#endif
	int status;
	int loopback =1;
	if(loopbackOn)
	{
#ifdef DEBUG
		printf("#INFO: Loopback enabled\n");
#endif
		status = setsockopt(*s,SOL_CAN_RAW,CAN_RAW_LOOPBACK,&loopback,sizeof(loopback));
		error_report(status);
	}
	addr->can_ifindex = ifr->ifr_ifindex;
	printf("#INIT CAN SUCCEEDED\n");
	return 0;
}


//***************************************************//
// *s		= the pointer to the socket we will
//	  	  be using
// can_addr	= struct that holds the properties 
//		  for the interface
//***************************************************//
int bind_can_socket(int *s,struct sockaddr_can can_addr){
	int status = bind(*s, (struct sockaddr *)&can_addr, sizeof(can_addr));
	error_report(status);
	printf("#BIND SUCCESFULL\n");
	return 0;
}

//*************************************************//
// *s		= the pointer to the socket we 
//		  will be using for the can com
// *addr	= the struct that holds the can
//		  properties
// identifier	= this is the ID that will be used
//		  for writing data on the bus
// *Data	= pointer to the data that we will
//		  be sending
//*************************************************//
int write_can_frame_ascii(int* s, struct sockaddr_can* addr,int identifier,unsigned char* Data){
	struct	can_frame frame;
	int status,dataSize;
	frame.can_id = identifier;

	int i,totalPackets,totalFullPackets,remainingDataIndex,sizeOfLast = 0;
	char Buf[8];
	dataSize = strlen(Data);
	totalPackets = dataSize / 8;
	if((dataSize % 8 !=0)){
		totalPackets++;
		totalFullPackets = totalPackets -1;
		sizeOfLast = dataSize - ((totalPackets - 1) * 8);
	}
	else totalFullPackets = totalPackets;

	for(i = 0; i < totalFullPackets;i++){
		strncpy(Buf,&Data[i*8],8);
		strncpy(frame.data,Buf,8);
		frame.can_dlc = 8;
		status = write(*s,&frame,sizeof(frame));
		error_report(status);
	}
	if(totalFullPackets != totalPackets){
		memset(Buf,0,sizeof(Buf));
		remainingDataIndex = dataSize - (dataSize - (i*8));
		strncpy(Buf,&Data[remainingDataIndex],sizeOfLast);
		strncpy(frame.data,Buf,sizeOfLast);
		frame.can_dlc = strlen(Buf);
		status = write(*s,&frame,sizeof(frame));
		error_report(status);
	}
#ifdef DEBUG
	printf("#INFO LIB: datalength: %d\n",frame.can_dlc);
	printf("#INFO LIB: data: %s\n",Data);
	printf("#INFO LIB: data id: %d\n",frame.can_id);
#endif

	return frame.can_dlc; //returns the datalength
}


int write_can_frame(int *s,struct sockaddr_can* addr,int identifier,__u8 Data[8])
{
	struct can_frame frame;
	int i;
	int status,dataSize;
#ifdef DEBUG
	printf("#INFO LIB: Write CAN FRAME: Input DATA:\n");
	for(i = 0; i< sizeof(Data); i ++)
	{
		printf("- %x ",Data[i]);
	}
	printf("\n");
#endif

	memcpy(frame.data,Data,sizeof(Data));

	frame.can_id = identifier;
	frame.can_dlc = 8;
	status = write(*s,&frame,sizeof(frame));
	error_report(status);

#ifdef DEBUG
	printf("#INFO LIB: datalength: %d\n",frame.can_dlc);
	printf("#INFO LIB: data:\n");
	for(i = 0; i< frame.can_dlc; i ++)
	{
		printf("- %x ",frame.data[i]);
	}
	printf("\n#INFO LIB: data id: %d\n",frame.can_id);
#endif

	return frame.can_dlc;
}



//Identifier filter will remove all packets with another identifier. If 0 everything will be read

//******************************************
// *s		= pointer to the socket
// sockaddr_can	= properties of interface
// buffer	= array which will contain
//		  the data
// idFilter	= Will allow a filter on
//		  the incoming id
//*****************************************
int read_can_frame(int *s,struct sockaddr_can *socketProperties, __u8 buffer[8],int identifierFilter){
	int length = 0;
	int returnVal = 0;
	int i;
	struct can_frame frame;
	memset(buffer,0,sizeof(buffer));
	length = recv(*s,&frame,sizeof(frame),0);
	error_report(length);

#ifdef DEBUG
	printf("#INFO LIB: Packet received from: %d\n",frame.can_id); //DEBUG ID

	printf("#INFO LIB: datalength: %d\n",length); 	//DEBUG DATA LENGTH

	printf("#INFO LIB: data: ");                  	//DEBUG DATA
	for(i = 0; i < frame.can_dlc; i ++)
	{
		printf("- %x ",frame.data[i]);
	}
#endif

	if(identifierFilter == frame.can_id || identifierFilter == 0)
	{
		memcpy(buffer,frame.data,frame.can_dlc);
		returnVal = 1;
	}
	else
	{
		return 0;
	}
	return returnVal;
}
