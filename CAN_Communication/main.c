/*
 * main.c
 *
 *  Created on: Nov 7, 2013
 *      Author: bvl
 */

#include "can_lib.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

int main(){
	int s,status,i;
	struct sockaddr_can can_addr;
	struct ifreq ifr;
	__u8 readBuffer[8];
	__u8 writeBuffer[8];

	memset(readBuffer,0,sizeof(readBuffer));
	memset(writeBuffer,0,sizeof(writeBuffer));
	status = load_can_modules(0);
	status = init_can_socket(&can_addr,&s,&ifr,0);
	error_report(status);
	status = bind_can_socket(&s,can_addr);
	error_report(status);

	writeBuffer[0] = 0x00;
#ifdef DEBUG
	for(i = 0; i< sizeof(writeBuffer); i ++)
	{
		printf("- %x ",writeBuffer[i]);
	}
	printf("\n");
#endif


	for(;;){

		printf("#WRITING DATA\n");
		status = write_can_frame(&s,&can_addr,ID_KLAXON,writeBuffer);	//EXAMPLE WRITE
		error_report(status);

		/*printf("#READING DATA\n");
		status = read_can_frame(&s,&can_addr,&readBuffer,0);							//EXAMPLE READ
#ifdef DEBUG
		if(status == 1)
			printf("Packet received for ID\n");
		else if(status == 0)
			printf("Packet received for other ID\n");

#endif*/
		//sleep(1); //DELAY
	}

	status = close(s);
	error_report(status);

	printf("#CLOSE SOCKED SUCCEEDED\n");

	return 0;
}

